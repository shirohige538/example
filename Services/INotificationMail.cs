﻿namespace WebApp.Services
{
    public interface INotificationMail
    {
        void SendNotification(string to, string subject, string from);
    }
}