﻿namespace WebApp.Services
{
    public class TestNotificationMail : INotificationMail
    {
        private readonly ILogger<TestNotificationMail> logger;

        public TestNotificationMail(ILogger<TestNotificationMail> logger)
        {
            this.logger = logger;
        }
        public void SendNotification(string to, string subject, string from)
        {
            logger.LogInformation($"To: {to}, Subject: {subject}, From: {from}");
        }
    }
}
