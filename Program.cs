using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.Reflection;
using System.Text;
using WebApp.Data;
using WebApp.Data.Entities;
using WebApp.Services;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddIdentity<UserInfo, IdentityRole>(cfg =>
{
    cfg.User.RequireUniqueEmail = true;
    cfg.Password.RequiredLength = 10;
}).AddEntityFrameworkStores<TestContext>();
//builder.Services.AddAuthentication().AddCookie().AddJwtBearer(cfg =>
//{
//    cfg.TokenValidationParameters = new TokenValidationParameters()
//    {
        //ValidIssuer = config["Token:Issuer"],
        //ValidAudience = config["Token:Audience"],
        //IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Token:Key"]))
//    };
//});
builder.Services.AddDbContext<TestContext>(cfg =>
{
    cfg.UseSqlServer();
});
builder.Services.AddTransient<INotificationMail, TestNotificationMail>();
builder.Services.AddScoped<ITestRepos, TestRepos>();
builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());
builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation().AddNewtonsoftJson(cfg => cfg.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseAuthorization();
app.UseEndpoints(cfg =>
{
    app.MapRazorPages();
    app.MapControllerRoute("Default", "{controller}/{action}/{id?}", new { controller = "Test", action = "Index" });
});


app.Run();
