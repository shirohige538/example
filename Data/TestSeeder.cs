﻿using Microsoft.AspNetCore.Identity;
using System.Text.Json;
using WebApp.Data.Entities;

namespace WebApp.Data
{
    public class TestSeeder
    {
        private readonly TestContext ctx;
        private readonly IWebHostEnvironment env;
        private readonly UserManager<UserInfo> manager;

        public TestSeeder(TestContext ctx, IWebHostEnvironment env, UserManager<UserInfo> manager)
        {
            this.ctx = ctx;
            this.env = env;
            this.manager = manager;
        }
        public async Task SeedAsync()
        {
            ctx.Database.EnsureCreated();
            UserInfo info = await manager.FindByEmailAsync("shirohige538@gmail.com");
            if(info == null)
            {
                info = new UserInfo()
                {
                    Username = "shirohige538@gmail.com",
                    Email = "shirohige538@gmail.com"
                };
            }
            var newUser = await manager.CreateAsync(info, "n01de@WhyImDoingTh1s");
            if(newUser != IdentityResult.Success)
            {
                throw new InvalidOperationException("Failed To Create Your User, Please Try Again");
            }
            if(!ctx.Products.Any())
            {
                var path = Path.Combine(env.ContentRootPath, "Data/test.json");
                var json = File.ReadAllText(path);
                var products = JsonSerializer.Deserialize<IEnumerable<Product>>(json);
                ctx.Products.AddRange(products);
                var odr = ctx.Orders.Where(i => i.Id == 1).FirstOrDefault();
                if(odr != null)
                {
                    odr.Info = info;
                    odr.Items = new List<OrderItem>()
                    {
                        new OrderItem()
                        {
                            Product = products.First(),
                            Quantity = 10,
                            UnitPrice = products.First().Price
                        }
                    };
                };
                ctx.Orders.Add(odr);
                ctx.SaveChanges();
            }
        }
    }
}
