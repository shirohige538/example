﻿using WebApp.Data.Entities;

namespace WebApp.Data
{
    public interface ITestRepos
    {
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetProductsByCategory(string category);
        bool SaveAll();
        void AddEntity(object order);
        IEnumerable<Order> GetOrders(bool includeItems);
        Order GetOrderById(string username, int id);
        IEnumerable<Order> GetOrdersByUser(string username, bool includeItems);
    }
}