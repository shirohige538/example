﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Entities;

namespace WebApp.Data
{
    public class TestRepos : ITestRepos
    {
        private readonly TestContext ctx;
        private readonly ILogger lg;

        public TestRepos(TestContext ctx, ILogger<TestRepos> lg)
        {
            this.ctx = ctx;
            this.lg = lg;
        }
        public IEnumerable<Product> GetProducts()
        {
            try
            {
                lg.LogInformation("Good");
                return ctx.Products.OrderBy(p => p.Title).ToList();
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<Product> GetProductsByCategory(string category)
        {
            return ctx.Products.Where(p => p.Category == category).ToList();
        }
        public bool SaveAll()
        {
            return ctx.SaveChanges() > 0;
        }
        public void AddEntity(object order)
        {
            ctx.Add(order);
        }
        public IEnumerable<Order> GetOrders(bool includeItems)
        {
            if (includeItems)
            {
                return ctx.Orders.Include(i => i.Items).ThenInclude(p => p.Product).ToList();
            }
            else
            {
                return ctx.Orders.ToList();
            }
        }
        public Order GetOrderById(string username, int id)
        {
            return ctx.Orders.Include(i => i.Items).ThenInclude(p => p.Product).Where(d => d.Id == id && d.Info.UserName == username).FirstOrDefault();
        }
        public IEnumerable<Order> GetOrdersByUser(string username, bool includeItems)
        {
            if (includeItems)
            {
                return ctx.Orders.Where(i => i.Info.UserName == username).Include(i => i.Items).ThenInclude(p => p.Product).ToList();
            }
            else
            {
                return ctx.Orders.Where(i => i.Info.UserName == username).ToList();
            }
        }
    }
}
