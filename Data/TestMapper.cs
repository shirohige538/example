﻿using AutoMapper;
using WebApp.Data.Entities;
using WebApp.ViewModels;

namespace WebApp.Data
{
    public class TestMapper : Profile
    {
        public TestMapper()
        {
            CreateMap<Order, OrderViewModel>().ForMember(o => o.OrderId, m => m.MapFrom(i => i.Id)).ReverseMap();
            CreateMap<OrderItem, OrderItemViewModel>().ReverseMap();
        }
    }
}
