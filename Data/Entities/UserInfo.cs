﻿using Microsoft.AspNetCore.Identity;

namespace WebApp.Data.Entities
{
    public class UserInfo : IdentityUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
