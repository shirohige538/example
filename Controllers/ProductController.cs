﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Data;
using WebApp.Data.Entities;

namespace WebApp.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductController : Controller
    {
        private readonly ITestRepos repos;
        private readonly ILogger<ProductController> logger;

        public ProductController(ITestRepos repos, ILogger<ProductController> logger)
        {
            this.repos = repos;
            this.logger = logger;
        }
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Product>> Get()
        {
            try
            {
                return Ok(repos.GetProducts());
            }
            catch (Exception ex)
            {
                logger.LogError($"Not Good, {ex} Had Failed");
                return BadRequest("Failed To Load");
            }
        }
    }
}
