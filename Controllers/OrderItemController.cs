﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data;
using WebApp.Data.Entities;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Route("api/orders/{orderid}/items")]
    public class OrderItemController : Controller
    {
        private readonly ITestRepos repos;
        private readonly ILogger<OrderItemController> log;
        private readonly IMapper map;

        public OrderItemController(ITestRepos repos, ILogger<OrderItemController> log, IMapper map)
        {
            this.repos = repos;
            this.log = log;
            this.map = map;
        }
        [HttpGet]
        public IActionResult Get(int orderId)
        {
            var order = repos.GetOrderById(User.Identity.Name, orderId);
            if (order != null)
            {
                return Ok(map.Map<IEnumerable<OrderItem>, IEnumerable<OrderItemViewModel>>(order.Items));
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet("{id}")]
        public IActionResult Get(int orderId, int id)
        {
            var order = repos.GetOrderById(User.Identity.Name, orderId);
            if (order != null)
            {
                var item = order.Items.Where(i => i.Id == id).FirstOrDefault();
                if (item != null)
                {
                    return Ok(map.Map<OrderItem, OrderItemViewModel>(item));
                }
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
