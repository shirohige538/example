﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApp.Data;
using WebApp.Data.Entities;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> log;
        private readonly SignInManager<UserInfo> manager;
        private readonly UserManager<UserInfo> user;
        private readonly IConfiguration config;

        public AccountController(ILogger<AccountController> log, SignInManager<UserInfo> manager, UserManager<UserInfo> user, IConfiguration config)
        {
            this.log = log;
            this.manager = manager;
            this.user = user;
            this.config = config;
        }
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Test");
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await manager.PasswordSignInAsync(model.Username, model.Password, model.Remember, false);
                if (result.Succeeded)
                {
                    if (Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        return Redirect(Request.Query["ReturnUrl"].First());
                    }
                    else
                    {
                        return RedirectToAction("Notification", "Test");
                    }
                }
            }
            ModelState.AddModelError("", "Failed To Login, Please Try Again In A Few Minutes");
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await manager.SignOutAsync();
            return RedirectToAction("Index", "Test");
        }
        [HttpPost]
        public async Task<IActionResult> TokenCreation([FromBody]LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var info = await user.FindByNameAsync(model.Username);
                if (user != null)
                {
                    var result = await manager.CheckPasswordSignInAsync(info, model.Password, false);
                    if (result.Succeeded)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, info.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.UniqueName, info.Username)
                        };
                        var signatureKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Token:Key"]));
                        var credential = new SigningCredentials(signatureKey, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(config["Token:Issuer"], config["Token:Audience"], claims, signingCredentials: credential, expires: DateTime.UtcNow.AddDays(2));
                        return Created("", new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        });
                    }
                }
            }
            return BadRequest();
        }
    }
}
