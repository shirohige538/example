﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data;
using WebApp.Data.Entities;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly ITestRepos repos;
        private readonly ILogger<OrderController> logger;
        private readonly IMapper mapper;
        private readonly UserManager<UserInfo> manager;

        public OrderController(ITestRepos repos, ILogger<OrderController> logger, IMapper mapper, UserManager<UserInfo> manager)
        {
            this.repos = repos;
            this.logger = logger;
            this.mapper = mapper;
            this.manager = manager;
        }
        [HttpGet]
        public IActionResult Run(bool includeItems)
        {
            try
            {
                return Ok(repos.GetOrders(includeItems));
            }
            catch (Exception ex)
            {
                logger.LogError($"Uh Oh, Seem Like {ex} Had Failed");
                return BadRequest("Failed To Run");
            }
        }
        [HttpGet]
        public IActionResult Get(bool includeItems = true)
        {
            try
            {
                var username = User.Identity.Name;
                var result = repos.GetOrdersByUser(username, includeItems);
                return Ok(mapper.Map<IEnumerable<OrderViewModel>>(result));
            }
            catch (Exception ex)
            {

                logger.LogError($"Unable To Get Order Because {ex} Had Failed");
                return BadRequest("Failed To Get Order");
            }
        }
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var odr = repos.GetOrderById(User.Identity.Name, id);
                if (odr != null) return Ok(mapper.Map<Order, OrderViewModel>(odr));
                else return NotFound();
            }
            catch (Exception ex)
            {
                logger.LogError($"Uh Oh, Look Like {ex} Had Failed");
                return BadRequest("Failed To Execute");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderViewModel order)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var refinedOrder = mapper.Map<OrderViewModel, Order>(order);
                    if (refinedOrder.OrderDate == DateTime.MinValue)
                    {
                        refinedOrder.OrderDate = DateTime.Now;
                    }
                    var currentUser = await manager.FindByNameAsync(User.Identity.Name);
                    refinedOrder.Info = currentUser;
                    repos.AddEntity(refinedOrder);
                    if (repos.SaveAll())
                    {
                        var model = new OrderViewModel()
                        {
                            OrderId = refinedOrder.Id,
                            OrderDate = refinedOrder.OrderDate,
                            OrderNumber = refinedOrder.OrderNumber
                        };
                        return Created($"/api/order/{refinedOrder.Id}", mapper.Map<Order, OrderViewModel>(refinedOrder));
                    }
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"No New Order For You Because {ex} Had Failed");
            }
            return BadRequest("Failed To Create Order");
        }
    }
}
