﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data;
using WebApp.Services;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class TestController : Controller
    {
        private readonly INotificationMail _notificationMail;
        private readonly ITestRepos repos;

        public TestController(INotificationMail notificationMail, ITestRepos repos)
        {
            _notificationMail = notificationMail;
            this.repos = repos;
        }
        public IActionResult Index()
        {
            //var result = ctx.Products.ToList();
            return View();
        }
        [HttpGet("notification")]
        //[Authorize(AuthenticationSchemes=JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Notification()
        {
            return View();
        }
        [HttpPost("notification")]
        public IActionResult Notification(NotificationViewModel model)
        {
            if (ModelState.IsValid)
            {
                _notificationMail.SendNotification("shirohige538@gmail.com", "Thank You For Registering Our Notifications", $"From: {model.Name}");
                ViewBag.UserMessage = "Notification Mail Sent";
                ModelState.Clear();
            }
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult Store()
        {
            var display = repos.GetProducts();
            return View(display);
        }
    }
}
