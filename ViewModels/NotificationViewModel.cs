﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels
{
    public class NotificationViewModel
    {
        [Required]
        [MinLength(5)]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
