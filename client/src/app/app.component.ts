import { Component } from '@angular/core';

@Component({
  selector: 'notification',
  templateUrl: "app.component.html",
  styles: []
})
export class AppComponent {
  title = 'Notification';
}
