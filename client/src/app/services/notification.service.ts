﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Order, OrderItem } from "../shared/Order";
import { Product } from "../shared/Product";
@Injectable()
export class Notification {
    constructor(private http: HttpClient) {

    }
    public products: Product[] = [];
    public orders: Order = new Order();
    loadProducts(): Observable<void> {
        return this.http.get<[]>("/api/products").pipe(map(data => {
            this.products = data;
            alert("abc");
            return;
        }));
    }
    addToOrder(product: Product) {
        const cartItem = new OrderItem();
        cartItem.productId = product.id;
        cartItem.productTitle = product.title;
        cartItem.productArtId = product.artId;
        cartItem.productArtist = product.artist;
        cartItem.productCategory = product.category;
        cartItem.productSize = product.size;
        cartItem.unitPrice = product.price;
        cartItem.quantity = 1;

        this.orders.items.push(cartItem);
    }
}