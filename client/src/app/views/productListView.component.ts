﻿import { Component, OnInit } from "@angular/core";
import { Notification } from "../services/notification.service";

@Component({
    selector: "example-list",
    templateUrl: "productListView.component.html",
    styleUrls: ["productListView.component.css"]
})
export default class ProductListView implements OnInit {
    constructor(public notify: Notification) {

    }
    ngOnInit(): void {
        this.notify.loadProducts().subscribe(() => {

        });
    }
}