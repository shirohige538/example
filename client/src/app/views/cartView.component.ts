﻿import { Component } from "@angular/core";
import { Notification } from "../services/notification.service";

@Component({
    selector: "recommendation",
    templateUrl: "cartView.component.html",
    styleUrls: ["cartView.component.css"]
})
export class CartView {
    constructor(public notifi: Notification) {

    }
}